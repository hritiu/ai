from domain.problem import *
from controller.controller import *

class Ui:
    def __init__(self,inputFile):
        self.file=inputFile
        self.problem=Problem(self.file)
        self.ctrl=Controller(self.problem)

    def printMenu(self):
        print("1.Find path with BFS\n"+"2.Find path with GBFS\n"+"3.Exit")

    def BFS(self):
        returnedValue=self.ctrl.bfs(self.problem.getMatrix())
        if returnedValue == 0:
            print("There is no solution! \n")
        else:
            for value in returnedValue:
                print(value)

    def GBFS(self):
        returnedValue=self.ctrl.gbfs(self.problem.getMatrix())
        if returnedValue == 0:
            print("There is no solution! \n")
        else:
            for value in returnedValue:
                print(value)

    def run(self):
        stop=False
        while not stop:
            self.printMenu()
            keyboardInput=input()
            if keyboardInput=='1':
                self.BFS()
            elif keyboardInput=='2':
                self.GBFS()
            elif keyboardInput=='3':
                stop=True
            else:
                print("Invalid option! Please try again! \n")
