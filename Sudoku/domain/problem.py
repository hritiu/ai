from math import sqrt
from copy import  deepcopy

class Problem:
    def __init__(self,inputFile):
        self.inputFile=inputFile
        self.matrix=self.readFromFile(self.inputFile)

        self.length=len(self.matrix)
        self.quadrantLength=sqrt(self.length)

    def readFromFile(self,fileName):
        f=open(fileName,'r')
        #list=[[int(number) for number in line.split(',')] for line in f]
        list = [[int(num) for num in line.split(',')] for line in f if line.strip() != ""]
        return list

    def returnFirstEmptyLocation(self,mat):
        for i in range(0,self.length):
            for j in range(0,self.length):
                if(mat[i][j]==0):
                    return i,j
        return -1,-1

    def getMatrix(self):
        return self.matrix

    def getLength(self):
        return self.length

    def isResult(self,currentState):
        frequency=[]
        for j in range(0, self.length +1):
            frequency.append(0)

        #we check if there exist duplicates on a line
        for i in range(0,self.length):
            for j in range(0,self.length+1):
                frequency[j]=0

            for j in range(0,self.length):
                frequency[currentState[i][j]]+=1

            for j in range(0,self.length+1):
                if frequency[j]>1 or frequency[0]>0:
                    return False

        #we check if there exist duplicates on a column
        for j in range(0,self.length):
            for i in range(0,self.length+1):
                frequency[i]=0

            for i in range(0,self.length):
                frequency[currentState[i][j]]+=1

            for i in range(0,self.length+1):
                if frequency[i]>1 or frequency[0]>0:
                    return False

        #we check if there exist duplicates in a quadrant
        for l in range(0,self.length,int(self.quadrantLength)):
            for c in range(0,self.length,int(self.quadrantLength)):
                for i in range(0,self.length+1):
                    frequency[i]=0

                for i in range(l,l+int(self.quadrantLength)):
                    for j in range(c,c+int(self.quadrantLength)):
                        frequency[currentState[i][j]]+=1

                for i in range(0, self.length +1):
                    if frequency[i]>1  or frequency[0]>0:
                        return False

        return True

    def getPossibleValues(self,mat):
        values=[]
        for i in range(1,self.length+1):
            values.append(i)

        i,j=self.returnFirstEmptyLocation(mat)
        if(i!=-1):
            for k in range(0,self.length):
                if mat[i][k] in values:
                    values.remove(mat[i][k])
                if mat[k][j] in values:
                    values.remove(mat[k][j])

            lQuadrant=int(int(i/self.quadrantLength)*self.quadrantLength)
            cQuadrant=int(int(j/self.quadrantLength)*self.quadrantLength)

            for k in range(0,int(self.quadrantLength)):
                for l in range(0,int(self.quadrantLength)):
                    if mat[lQuadrant][cQuadrant] in values:
                        values.remove(mat[lQuadrant][cQuadrant])

            return values
        else:
            return []

    def expand(self,mat):
        list=[]
        allValues=self.getPossibleValues(mat)
        i,j=self.returnFirstEmptyLocation(mat)
        v=0

        for value in allValues:
            list.append(deepcopy(mat))
            list[v][i][j]=value
            v+=1

        return list

    def heuristic(self,mat1,mat2):
        valuesMat1=self.getPossibleValues(mat1)
        valuesMat2 = self.getPossibleValues(mat2)

        if len(valuesMat1)>len(valuesMat2):
            return 1
        return 0

    def heuristicForOneMatrix(self,mat):
        return len(self.getPossibleValues(mat))