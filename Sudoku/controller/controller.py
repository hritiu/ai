class Controller:
    def __init__(self,problem):
        self.__problem=problem

    def orderStates(self,states):
        for i in range(0,len(states)-1):
            for j in range(i+1,len(states)):
                if(self.__problem.heuristic(states[i],states[j])):
                    aux=states[i]
                    states[i]=states[j]
                    states[j]=aux

    def bfs(self,root):
        queue=[root]
        while len(queue)>0:
            currentState=queue.pop(0)
            if self.__problem.isResult(currentState):
                return currentState
            else:
                queue=queue+self.__problem.expand(currentState)
        return 0

    def gbfs(self,root):
        queue=[root]
        while len(queue)>0:
            currentState=queue.pop(0)
            if self.__problem.isResult(currentState):
                return currentState
            val=self.__problem.expand(currentState)
            self.orderStates(val)

            if len(val) > 1:
                queue = queue + [val[0]]

        return 0